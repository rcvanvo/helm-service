# Basic Spring Boot Kubernetes Helm Boilerplate

The goal with this repository was to provide the basic starting point for a simple [Spring Boot](https://projects.spring.io/spring-boot/) service that will be deployed into [Kubernetes](https://kubernetes.io/) using [Helm](https://helm.sh/).  Most of the examples out there don't take into account the full configuration options of Kubernetes, and instead overly simplify the deployment with *just* the deployment yaml, causing a little bit of confusion for defining `ConfigMap`, `Service`, and `Ingress` as well as also providing a reasonable example of `Secrets` storage.  All examples found online incorporated some other tooling, rather than keeping the complexity of the architecture low.

## Building

There are three levels of building in a typical Kubernetes Helm deployment:

 * Building the Application
 * Building the Container, and placing the Application into the Container
 * *"Compiling"* the Helm Chart (Note: Helm Charts are just configuration, and do not contain actual source code, merely they are bundled up templates for Kubernetes configuration files)

### Building the Application

Since this example is using Spring Boot, it's leveraging the Gradle build system.  Rather than use the gradle wrapper and create more boilerplate bloat, the build is handled by Docker.

```
docker run --rm -v "$PWD":/home/gradle/project:Z -w /home/gradle/project gradle build
```

### Building the Container

This requires docker, and should be done **after** building the application, or else it'll fail.

```
docker build .
```

### Building the Chart

The Helm chart doesn't really get *built*, it's linted and verified so that during deployment it can be properly upgraded into the Kubernetes cluster.

```
helm init --client-only

helm lint charts/service

helm template -f charts/service/values.yaml charts/service
```

If you don't have helm, you can install it by doing this:

```
export HELM_VERSION=2.8.2
curl -s "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
mv linux-amd64/helm .
rm -rf linux-amd64/
./helm version --client
```

It's recommended to move that into your `/usr/local/bin` or `~/bin` for future use.

## Installing

Once the application and container are built, you'll really want to test deploying this into a Kubernetes cluster to make sure it's all functional.  The *easiest* way is to use an already existing cluster, but if you're unable to do that then follow the later instructions.

This cluster needs to have the following:

    * Helm Tiller
    * Prometheus

If it has those, then you just do this:

```
helm install charts/service
```

### Creating a temporary cluster

If you don't have a Kubernetes cluster to test this one, you can create your own using [Minikube](https://kubernetes.io/docs/getting-started-guides/minikube/)

This assumes you're on a Linux machine.

```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube
curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && chmod +x kubectl
```

It's recommended to move `minikube` into `/usr/local/bin` and `kubectl` into your `~/bin` directory. If you don't, you'll need to add `./` in front of the `minikube` and `kubectl` calls.

```
export MINIKUBE_WANTUPDATENOTIFICATION=false
export MINIKUBE_WANTREPORTERRORPROMPT=false
export MINIKUBE_HOME=$HOME
export CHANGE_MINIKUBE_NONE_USER=true
mkdir $HOME/.kube || true
touch $HOME/.kube/config

export KUBECONFIG=$HOME/.kube/config
sudo -E /usr/local/bin/minikube start --vm-driver=none --extra-config=kubelet.CgroupDriver=systemd

# this for loop waits until kubectl can access the api server that Minikube has created
for i in {1..150}; do # timeout for 5 minutes
   kubectl get po &> /dev/null
   if [ $? -ne 1 ]; then
      break
  fi
  sleep 2
done

# kubectl commands are now able to interact with Minikube cluster
```

Then we need to install the helm tiller:

```
helm install stable/nginx-ingress -f minikube/ingress.yaml
```

Next, we need an Ingress controller so we can access both our service and Grafana:

```
helm install 
And last, we'll use a development copy of Prometheus/Grafana so we can get up and running fastest:



## Overview

### Application

Right now the Spring Boot application is incredibly minimal to provide the best flexibility with defining additional functionality.  This is intentional, as modifying the service itself is outside the scope of this repository.  The application itself could be easily modified to become a Node.JS or other program by merely changing the application-specific configuration, the `Dockerfile`, and the accompanying `.gitlab-ci.yml`

### Container

All Docker related configuration is kept in the `Dockerfile`.

### Kubernetes Configuration

This is all kept in the `charts` directory and follows a standard Helm format.  This example **does not** externalize the configuration for deployments, which is an improvement that is **highly recommended** to support different environments.