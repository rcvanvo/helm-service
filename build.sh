#!/usr/bin/env bash

docker run --rm -v "$PWD":/home/gradle/project:Z -w /home/gradle/project gradle build

docker run --rm -v "$PWD":/home/gradle/project:Z -w /home/gradle/project gradle test

docker version

docker build -t planetvoor:service -f Dockerfile .

helm init --client-only

helm lint charts/service

helm template -f charts/service/values.yaml charts/service